Policies concerning the Alexa app called "Belgian vaccination stats"

# Privacy policy

This skill does not collect nor store any personal information about its users.

# Terms of Use

Everyone is free to use this skill as she or he likes.

This skill comes with no guarantee on the correctness of the provided data.

The data is based on https://epistat.wiv-isp.be/covid/

